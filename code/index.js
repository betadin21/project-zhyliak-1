try {
    getProducts("/json/productsAll.json")
    // будуємо товари кексів
    async function getProducts(url) {
        const response = await fetch(url)
        const json = await response.json()
        renderProducts(json)
    }
    const cartAmount = document.querySelector(".cart-amount");
    // відображаємо кількість товару в кошику
    if(localStorage.getItem("cart")){
        const items = JSON.parse(localStorage.getItem("cart"))
        let totalAm = 0;
        items.forEach(el=>{
            totalAm+= parseFloat(el.counter) 
        })
        cartAmount.innerText = totalAm;
    }
    const container = document.querySelector(".items-constainer")
    function renderProducts(itemsArr) {
        itemsArr.forEach(el => {
            const item = `
            <div class="item wow itemAll" data-id ="${el.id}">
                <div class="img"><img class="prod-img" src="/img/${el.img}.png" alt="cuocake"></div>
                <div class="name">${el.name}</div>
                <div class="ingredients">${el.discription}</div>
                <div class="price">$${el.price}</div>
                <div class="cart-container">
                    <div class="cart-item-number">
                        <div class="item-minus" data-id = "${el.id}">-</div>
                        <div class = "counter">1</div>
                        <div class="item-plus" data-id = "${el.id}">+</div>
                    </div>
                    <div class="cart" data-id = "${el.id}">ADD TO CART</div>
                </div>            
            </div>
            `
            container.insertAdjacentHTML("beforeend", item)
        });
    }
    //визначаємо кількість товару
    // масив для корзини
    let data = [];
    document.addEventListener("click", e => {
        if(e.target.classList.contains("cart")){
            //знаходжу карточку товара де був клік
            const card = e.target.closest(".item")
            // збираю дані з карточки товара де був клік
            const productInfo = {
                id: card.dataset.id,
		        imgSrc: card.querySelector('.prod-img').getAttribute('src'),
		        title: card.querySelector('.name').innerText,
		        ingredients: card.querySelector('.ingredients').innerText,
		        price: parseFloat(card.querySelector('.price').innerText.slice(1)),
                itemPrice: parseFloat(card.querySelector('.price').innerText.slice(1)),
		        counter: parseFloat(card.querySelector('.counter').innerText),
            }
            productInfo.price = productInfo.itemPrice * productInfo.counter;
            //перевіряю чи є вже дані в локальній памяті
            if(localStorage.getItem("cart")){
                let item = JSON.parse(localStorage.getItem("cart"))
                // шукаю об*єкт товару на якому був клік
                const itemIndex = item.findIndex(el=>{
                    return el.id == productInfo.id
                })
                // якщо об*єкт наявний - то змінюю його
                if(itemIndex >= 0){
                    const countNum = parseFloat(item[itemIndex].counter); 
                    // якщо кількість товару в корзині більше-рівна 1 то сумую кількість
                    if(countNum>=1){
                        item[itemIndex].counter = parseFloat(item[itemIndex].counter) + parseFloat(productInfo.counter)
                        item[itemIndex].price = item[itemIndex].counter * item[itemIndex].itemPrice
                    }
                    localStorage.setItem("cart", JSON.stringify(item))
                }
                // якщо об*єкта немає то додаємо 
                if(itemIndex === -1){
                    item.push(productInfo)
                    localStorage.setItem("cart", JSON.stringify(item))
                }
            }

            //якщо нема товару в корзині то додаємо
            if(!localStorage.getItem("cart")){
                data.push(productInfo)
                localStorage.setItem("cart", JSON.stringify(data))
            }
            // відображаємо кількість товару в кошику
            if(localStorage.getItem("cart")){
                const items = JSON.parse(localStorage.getItem("cart"))
                let totalAm = 0;
                items.forEach(el=>{
                    totalAm+= parseFloat(el.counter) 
                })
                cartAmount.innerText = totalAm;
            }
        }
    })

} catch (error) {
    console.error(error);
}

try {
    const container = document.querySelector(".check-items");
    const totalPriceDiv = document.querySelector(".total-price")
    const cartItems = JSON.parse(localStorage.getItem("cart"));
    // коли кошик пустий пишу "кошик порожній"
    const div = document.createElement("div");
    div.classList.add("empty-cart")
    div.innerText = "Your shopping cart is empty at the moment"
    if(!cartItems){
        if(!localStorage.getItem("cart")){
            container.insertAdjacentElement("beforeend", div)
        
        }
    }
    if(cartItems.length == 0){
        container.insertAdjacentElement("beforeend", div)
    }
    // виводжу товари в корзину на сторінку
    cartItems.forEach(cartItem=>{
        const item = `
                    <div class="check-item-container" data-id = ${cartItem.id}>
                        <div class="check-item-img"><img src="${cartItem.imgSrc}" alt="cake"></div>
                        <div class="check-item-name">${cartItem.title}</div>
                        <div class="check-item-acc cart-item-number">
                            <div class="item-minus" data-id = "${cartItem.id}">-</div>
                            <div class = "counter">${cartItem.counter}</div>
                            <div class="item-plus" data-id = "${cartItem.id}">+</div>
                        </div>
                        <div class="check-item-price">$${(cartItem.price).toFixed(2)}</div>
                        <div class="check-item-price delete-item">&#10060;</div>
                    </div>
                    `
                container.insertAdjacentHTML("beforeend", item)
    })
    // вираховую загальну вартість покупки
    let total = 0;
    cartItems.forEach(el=>{
        total+=el.price
    })
    totalPriceDiv.innerText = `$${total.toFixed(2)}`
    
} catch (error) {
    console.error(error);
}

document.addEventListener("click", e => {
    let counter;
    const cartAmount = document.querySelector(".cart-amount");
    //ловлю клік на кнопках плюс і мінус
    if(e.target.classList.contains("item-plus")||e.target.classList.contains("item-minus")){
        const wrapper = e.target.closest(".cart-item-number");
        counter =  wrapper.querySelector(".counter");
    }
    // клік на плюс
    if(e.target.classList.contains("item-plus")){
        counter.innerText = ++counter.innerText
        if(document.location.pathname == '/cart-page/'){
            const product = e.target.closest(".check-item-container"),
            productId = product.dataset.id
            const cartItem = JSON.parse(localStorage.getItem("cart"));
            const itemIndex = cartItem.findIndex(el=>{
                return el.id == productId
            })
            //збільшую каунтер на 1
            ++cartItem[itemIndex].counter;
            // розраховую вартість товару
            cartItem[itemIndex].price = cartItem[itemIndex].counter * cartItem[itemIndex].itemPrice;
            localStorage.setItem("cart", JSON.stringify(cartItem))
            location.reload()
        }
    }
    // клік на мінус
    if(e.target.classList.contains("item-minus")){
        counter.innerText = --counter.innerText
        if(document.location.pathname == '/cart-page/'){
            const product = e.target.closest(".check-item-container"),
            productId = product.dataset.id
            const cartItem = JSON.parse(localStorage.getItem("cart"));
            const itemIndex = cartItem.findIndex(el=>{
                return el.id == productId
            })
            //зменшую каунтер на 1
            --cartItem[itemIndex].counter;
            // розраховую вартість товару
            cartItem[itemIndex].price = cartItem[itemIndex].counter * cartItem[itemIndex].itemPrice;
            localStorage.setItem("cart", JSON.stringify(cartItem))
            location.reload()
        }
        if(counter.innerText<1){
            //видаляємо товар з корзини коли кількість менше 1
            if(document.location.pathname == '/cart-page/'){
                const product = e.target.closest(".check-item-container"),
                productId = product.dataset.id
                const cartItem = JSON.parse(localStorage.getItem("cart"));
                const itemIndex = cartItem.findIndex(el=>{
                    return el.id == productId
                })
                cartItem.splice([itemIndex],1);
                location.reload()
                localStorage.setItem("cart", JSON.stringify(cartItem))
            }else{
                counter.innerText = 1;
            }
        }
       
    }
})

//видаляємо елемент з корзини
document.addEventListener("click", (e)=>{
     if(e.target.classList.contains("delete-item")){
        const product = e.target.closest(".check-item-container"),
        productId = product.dataset.id
        const cartItem = JSON.parse(localStorage.getItem("cart"));
        const itemIndex = cartItem.findIndex(el=>{
            return el.id == productId
        })
        cartItem.splice([itemIndex],1);
        location.reload()
        localStorage.setItem("cart", JSON.stringify(cartItem))
    }
})
// переходимо на сторінку оплати
document.addEventListener("click", (e)=>{
    if(e.target.classList.contains("button-buy") || e.target.classList.contains("button-a")){
      document.location.href = "https://www.ipay.ua/ua/p2p?gclid=Cj0KCQjwxYOiBhC9ARIsANiEIfYrjY7HHga8kvTdZobe6sDNFAM_UKVIweSfECz7x8bLXL1wVJdrZ1EaArW-EALw_wcB"
    }
})
//бургер-меню
const burger = document.querySelector(".header-burger")
const headerMenu = document.querySelector(".header-menu")
const body = document.querySelector("body")
burger.addEventListener("click", ()=>{
    burger.classList.toggle("active")
    headerMenu.classList.toggle("active")
    body.classList.toggle("lock")
})


